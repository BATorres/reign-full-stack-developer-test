import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'
import {Observable} from 'rxjs';
import {HackerNewsInterface} from '../interfaces/hacker-news.interface';

@Injectable({
  providedIn: 'root'
})
export class HackerNewsService {
  protected readonly _httpClient;

  url = 'http://localhost:3000/';

  segmento = 'hacker-news';

  constructor(_httpClient: HttpClient) {
    this._httpClient = _httpClient;
  }

  findAll(take?: number, skip?: number): Observable<[any[]]> {
    const parameters = new HttpParams().set('take', take.toString()).set('skip', skip.toString());
    return this._httpClient.get(
      this.url + this.segmento, {
        params: parameters
      }
    );
  }

  deletePost(hackerNew: HackerNewsInterface) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })};

    return this._httpClient.put(
      this.url + this.segmento + '/delete-post', {hackerNew: hackerNew}, httpOptions
    );
  }
}
