import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {HackerNewsFeedComponent} from './routes/hacker-news-feed/hacker-news-feed.component';
import {CommonModule} from '@angular/common';
import {HackerNewsService} from './services/hacker-news.service';
import {PostDatePipe} from './pipes/post-date.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HackerNewsFeedComponent,
    PostDatePipe
  ],
  imports: [
    BrowserModule,
    CommonModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    HackerNewsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
