import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HackerNewsFeedComponent} from './routes/hacker-news-feed/hacker-news-feed.component';

const routes: Routes = [
  {
    component: HackerNewsFeedComponent,
    path: 'hn-feed',
  },
  {
    path: '',
    redirectTo: 'hn-feed',
    pathMatch: 'full',
  },
  {
    path: '**',
    redirectTo: 'hn-feed',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
