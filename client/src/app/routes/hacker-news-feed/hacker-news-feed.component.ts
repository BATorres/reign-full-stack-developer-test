import {Component, OnInit} from '@angular/core';
import {HackerNewsInterface} from '../../interfaces/hacker-news.interface';
import {HackerNewsService} from '../../services/hacker-news.service';

@Component({
  selector: 'app-hacker-news-feed',
  templateUrl: './hacker-news-feed.component.html',
  styleUrls: ['./hacker-news-feed.component.css']
})
export class HackerNewsFeedComponent implements OnInit {
  hackerNewsArray;

  constructor(
    private readonly _hackerNewsService: HackerNewsService,
  ) {
  }

  ngOnInit(): void {
    this.loadRecentHackerNews();
  }

  loadRecentHackerNews() {
    this._hackerNewsService.findAll(1000, 0)
      .subscribe(
        (serverResponse: [HackerNewsInterface[]]) => {
          this.hackerNewsArray = serverResponse;
          this.hackerNewsArray.filter(hackerNew => hackerNew.title !== null || hackerNew.story_title !== null);
        }
      )
  }

  onSelectedRow(hackerNew: HackerNewsInterface) {
    const url = hackerNew.story_url !== null ? hackerNew.story_url : hackerNew.url;
    window.open(url);
  }

  deletePost(hackerNew: HackerNewsInterface) {
    const index = this.hackerNewsArray.indexOf(hackerNew);
    this._hackerNewsService.deletePost(hackerNew)
      .subscribe(() => {

      }, error => {
        console.error({
          error,
          message: 'Error deleting post',
        })
      });
    return this.hackerNewsArray.splice(index, 1);
  }
}
