export interface HackerNewsInterface {
  _id?: string;
  author?: string;
  created_at?: string;
  comment_text?: string;
  num_comments?: number;
  objectID?: string;
  parent_id?: number;
  points?: string;
  story_id?: number;
  story_text?: string;
  story_title?: string;
  story_url?: string;
  tags?: string[];
  title?: string;
  url?: string;
}
