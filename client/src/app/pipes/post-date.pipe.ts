import {Pipe, PipeTransform} from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'postDate'})
export class PostDatePipe implements PipeTransform {
  transform(createdAt: string): string {
    let formatedDate;
    const dateToday = moment();
    const compareDates = dateToday.diff(createdAt, 'hours');
    if (compareDates < 24) {
      formatedDate = moment(createdAt).format('HH:mm a');
      return formatedDate;
    } else if (compareDates >= 24 && compareDates < 48) {
      formatedDate = 'Yesterday';
      return formatedDate;
    }
    else {
      formatedDate =  moment(createdAt).format('MMM D');
      return formatedDate;
    }
  }
}
