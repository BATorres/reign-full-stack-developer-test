import {Body, Controller, Get, Put, Query} from '@nestjs/common';
import {HackerNewsService} from './hacker-news.service';
import {HackerNewsEntity} from './hacker-news.entity';

@Controller('hacker-news')
export class HackerNewsController {
    constructor(
        private readonly _hackerNesService: HackerNewsService
    ) {}

    @Get()
    findAll(
        @Query() paginationOptions: {take: number, skip: number}
    ): Promise<HackerNewsEntity[]> {
        return this._hackerNesService.findAll(
            paginationOptions.take,
            paginationOptions.skip
        );
    }

    @Put('delete-post')
    deletePost(
        @Body('hackerNew') hackerNew
    ): Promise<any> {
        return this._hackerNesService.deletePost(
            hackerNew
        );
    }
}
