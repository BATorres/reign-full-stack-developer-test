import {HttpModule, Module} from '@nestjs/common';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsService } from './hacker-news.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {HackerNewsEntity} from './hacker-news.entity';

@Module({
  imports: [
      TypeOrmModule.forFeature([
          HackerNewsEntity
      ]),
      HttpModule,
  ],
  controllers: [HackerNewsController],
  providers: [HackerNewsService]
})
export class HackerNewsModule {}
