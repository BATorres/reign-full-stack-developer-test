import {Column, Entity, ObjectID, ObjectIdColumn} from 'typeorm';

@Entity()
export class HackerNewsEntity {
    @ObjectIdColumn()
    _id: ObjectID;

    @Column()
    objectID?: string;

    @Column()
    created_at_id?: number;

    @Column()
    story_id?: number;

    @Column()
    parent_id?: number;

    @Column()
    created_at?: string;

    @Column()
    title?: string;

    @Column()
    url?: string;

    @Column()
    author?: string;

    @Column()
    points?: string;

    @Column()
    story_text?: string;

    @Column()
    comment_text?: string;

    @Column()
    num_comments?: string;

    @Column()
    story_title?: string;

    @Column()
    story_url?: string;

    @Column()
    _tags?: string[];

    @Column()
    _highlightResult?: any;

    @Column()
    is_active?: 1 | 0;
}