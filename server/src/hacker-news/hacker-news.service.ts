import {HttpService, Injectable, OnModuleInit} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {HackerNewsEntity} from './hacker-news.entity';
import {MongoRepository} from 'typeorm';
import {Cron, CronExpression} from '@nestjs/schedule';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable()
export class HackerNewsService implements OnModuleInit {
    constructor(
        @InjectRepository(HackerNewsEntity)
        private readonly _hackerNewsRepository: MongoRepository<HackerNewsEntity>,
        private readonly _httpService: HttpService,
    ) {
    }

    @Cron(CronExpression.EVERY_HOUR)
    async verifyNewData() {
        const dataLocalApi: HackerNewsEntity[] = await this.findAll();
        const existDataLocalApi: boolean = dataLocalApi.length > 0;

        if (existDataLocalApi) {
            this.fetchData()
                .subscribe(
                    (response: { hits: HackerNewsEntity[] }) => {
                        for (let hackerNew of response.hits) {
                            const index: number = dataLocalApi
                                .map(hackerNew => hackerNew.objectID)
                                .indexOf(hackerNew.objectID);
                            if (index === -1) {
                                const hackerNewToSave: HackerNewsEntity = {...hackerNew, is_active: 1};
                                this._hackerNewsRepository.save(hackerNewToSave);
                            }
                        }
                    },
                    error => {
                        console.error({
                            error,
                            message: 'Error fetching hacker news data'
                        })
                    }
                );
        }
    }

    async onModuleInit() {
        const dataLocalApi: HackerNewsEntity[] = await this.findAll();
        const existDataLocalApi: boolean = dataLocalApi.length > 0;

        if (!existDataLocalApi) {
            this.fetchData()
                .subscribe(
                    (response: { hits: HackerNewsEntity[] }) => {
                        const dataToCreate = response.hits.map(hit => hit = {...hit, is_active: 1});
                        this._hackerNewsRepository.save(dataToCreate);
                    },
                    error => {
                        console.error({
                            message: 'Error fetching hacker news data'
                        })
                    }
                );
        }
    }

    fetchData(): Observable<any> {
        return this._httpService.get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs&hitsPerPage=1000')
            .pipe(
                map(
                    apiResponse => apiResponse.data
                )
            );
    }

    async findAll(take?: number, skip?: number): Promise<HackerNewsEntity[]> {
        return this._hackerNewsRepository.find({
            where: {
              is_active: 1
            },
            order: {
                created_at: 'DESC'
            },
            take: +take,
            skip: +skip,
        });
    }

    async deletePost(hackerNew: HackerNewsEntity): Promise<any> {
        return await this._hackerNewsRepository.updateOne(
            {'objectID': hackerNew.objectID},
            {$set: {is_active: 0}},
            );
    }
}
