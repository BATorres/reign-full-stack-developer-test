import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {AppService} from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import {HackerNewsModule} from './hacker-news/hacker-news.module';
import {HackerNewsEntity} from './hacker-news/hacker-news.entity';
import {ScheduleModule} from '@nestjs/schedule';

@Module({
    imports: [
        TypeOrmModule.forRoot({
            useNewUrlParser: true,
            type: 'mongodb',
            url: 'mongodb://andrestorres:12345678@mongo_db:27017/test',
            // url: 'mongodb://andrestorres:12345678@localhost:32770/test',
            authSource: 'admin',
            entities: [
                HackerNewsEntity
            ],
            synchronize: true,
            dropSchema: true
        }),
        ScheduleModule.forRoot(),
        HackerNewsModule,
    ],
    controllers: [AppController],
    providers: [AppService],
})
export class AppModule {
}
