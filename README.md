# Reign Full Stack Developer Test

## Prerequisites

- For the project to work correctly it is necessary to have Mongo Server in the machine in which the present project will be tested.
- File sharing permissions must be given in case Docker asks for them during execution.

## Notes

- The creation of data in the database is automated, so no additional configuration is required to configure the database.
- To verify the server API data, use the url **localhost:3000/hacker-news** to verify the existing data.

## Instructions

To test the present project you must:

1. Clone the repository in your local machine.
2. Inside the repository, in the path **reign-full-stack-developer-test/** you will found all the important archives of the project.
3. Run command **docker-compose up --build** in the path mentioned above to create and set up all  Docker containers for the project.
4. Finally, in any browser, enter **localhost:4200** to access to the application.
